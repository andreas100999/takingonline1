<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/home',['middleware' => 'admin',function(){
  return view('admin.index');
}]);
Route::get('/admin',function(){
	return view('admin/index');
});
Route::get('pemesan',function(){
	return view('index');
});
Route::get('pemilik',function(){
	return view('pemilik/index');
});
Route::post('kirimdata','loginController@masuk');
Route::post('prosesRegister','loginController@create');
Auth::routes();

Route::get('logout','loginController@keluar');
Auth::routes();

//taking
Route::get('/pesan','tampilRuteController@index');
Route::get('/rental',function(){
  return view('taking/rental');
});


//halaman isi saldo
Route::get('/isiSaldo',function(){
  return view('taking/isiSaldo');
});
Route::post('/prosesIsiSaldo/{id}','isiSaldoController@store');

//lanjut Pesan
Route::post('/prosesPesan','prosesPesanController@index');
Route::get('/prosesPesan1/{id}/{id_pemilikKendaraan}','prosesPesanController@store');
Route::get('coba',function(){
    return view('auth/login1');
});
//Lanjut rental
Route::post('/prosesRental','prosesPesanController@simpan');
Route::get('/prosesPesan2/{id}/{id_pemilikKendaraan}','prosesPesanController@store2');
