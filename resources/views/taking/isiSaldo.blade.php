<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Isi Saldo</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    @include('layout.nav')
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Isi Saldo</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Isi Saldo</li>
      </ol>
      <br><br>
      <div class="card-deck">
        <div class="card border-primary mb-3" style="max-width: 18rem;">
        <div class="card-header">Mastercard</div>
        <div class="card-body text-primary">
          <div class="img-thumbnail">
            <img src="images/ikon/mastercard.png" style="width:200px">
          </div>
        </div>
      </div>
      <div class="card border-primary mb-3" style="max-width: 18rem;">
        <div class="card-header">BRI</div>
        <div class="card-body text-primary">
          <div class="img-thumbnail">
            <img src="images/ikon/bri.png" style="width:200px">
          </div>
        </div>
      </div>
      <div class="card border-primary mb-3" style="max-width: 18rem;">
        <div class="card-header">Doku</div>
        <div class="card-body text-primary">
          <div class="img-thumbnail">
            <img src="images/ikon/doku.jpg" style="width:180px">
          </div>
        </div>
      </div>
      <div class="card border-primary mb-3" style="max-width: 18rem;">
      <div class="card-header">Header</div>
      <div class="card-body text-primary">
        <div class="img-thumbnail">
          <img src="images/ikon/danamon.png" style="width:200px">
        </div>
      </div>
    </div>
    </div>
    <div class="card-deck">
      <div class="card border-primary mb-3" style="max-width: 18rem;">
      <div class="card-header">visa</div>
      <div class="card-body text-primary">
        <div class="img-thumbnail">
          <img src="images/ikon/visa.png" style="width:200px">
        </div>
      </div>
    </div>
    <div class="card border-primary mb-3" style="max-width: 18rem;">
      <div class="card-header">BCA</div>
      <div class="card-body text-primary">
        <div class="img-thumbnail">
          <img src="images/ikon/bca.png" style="width:200px">
        </div>
      </div>
    </div>
    <div class="card border-primary mb-3" style="max-width: 18rem;">
      <div class="card-header">Mandiri</div>
      <div class="card-body text-primary">
        <div class="img-thumbnail">
          <img src="images/ikon/mandiri.jpg" style="width:200px">
        </div>
      </div>
    </div>
    <div class="card border-primary mb-3" style="max-width: 18rem;">
    <div class="card-header">BNI</div>
    <div class="card-body text-primary">
      <img src="images/ikon/bni.png" style="width:200px">
    </div>
  </div>
  </div>
  <br><br>

  <form action="/prosesIsiSaldo/{{ Auth::user()->id }}" enctype="multipart/form-data" method="post">
    @csrf
      <div class="form-group row">
        <label for="saldo" class="col-sm-2 col-form-label">Masukkan Saldo</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="saldo" placeholder="Saldo..">
        </div><br><br><br>
        <div class="col-sm-10">
          <input type="file" class="form-control-file" name="gambar">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
  </form>
    </div>
    <!-- /.container -->
    <br><br>
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
