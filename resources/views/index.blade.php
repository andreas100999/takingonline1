<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Taking Online</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    @include('layout/nav')
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('images/ikon/foto1.jpg')">
            <div class="carousel-caption d-none d-md-block">
              <h3>First Slide</h3>
              <p>This is a description for the first slide.</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('images/ikon/tujuan.png')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Second Slide</h3>
              <p>This is a description for the second slide.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('images/ikon/angkot.png')">
            <div class="carousel-caption d-none d-md-block">
              <h3>Third Slide</h3>
              <p>This is a description for the third slide.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->

    <div class="container">

      <h1 class="my-4">Selamat datang di Taking Online</h1>

      <!-- Marketing Icons Section -->
      <div class="row">
        <div class="col-lg-4 mb-4 ">
          <div class="card h-100 card bg-dark text-white">
            <h4 class="card-header">Taking-Car</h4>
            <div class="card-body">
              <center>
              	<img src="images/ikon/mobil.jpg" style="width: 200px" class="img-thumbnail">
              </center><br>
              <p>kami menyediakan driver yang siap mengantar kamu kemana saja,
              nggak perlu repot nyetir,atau cari parkir.Yuk pesan sekarang di Taking-Car</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Selanjutnya -></a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100 card bg-dark text-white">
            <h4 class="card-header">Taking-Ride</h4>
            <div class="card-body">
              <center>
              	<img src="images/ikon/kareta.png" style="width: 200px" class="img-thumbnail">
              </center>
              <br>
              <p>kami menyediakan driver yang siap mengantar kamu kemana saja,
              nggak perlu repot nyetir,atau cari parkir.Yuk pesan sekarang di Taking-Car</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Selanjutnya -></a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100 card bg-dark text-white">
            <h4 class="card-header">Taking-Angkot</h4>
            <div class="card-body">
              <center>
              	<img src="images/ikon/angkot.png" style="width: 200px" class="img-thumbnail">
              </center>
              <br>
              <p>kami menyediakan driver yang siap mengantar kamu kemana saja,
              nggak perlu repot nyetir,atau cari parkir.Yuk pesan sekarang di Taking-Car</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Selanjutnya -></a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <br><br><br>

      <!-- Features Section -->
      <div class="row">
        <div class="col-lg-6">
          <h2>Taking Online</h2>
          <p>The Modern Business template by Start Bootstrap includes:</p>
          <ul>
            <li>
              <strong>Bootstrap v4</strong>
            </li>
            <li>jQuery</li>
            <li>Font Awesome</li>
            <li>Working contact form with validation</li>
            <li>Unstyled page elements for easy customization</li>
          </ul>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
        </div>
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="images/ikon/logo.jpg" alt="">
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <!-- Call to Action Section -->
      <div class="row mb-4">
        <div class="col-md-8">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
        </div>
        <div class="col-md-4">
          <a class="btn btn-lg btn-primary btn-block" href="#">Ayo Gunakan</a>
        </div>
      </div>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    @include('layout/footer')

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>

</html>
