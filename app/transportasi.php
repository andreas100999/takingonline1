<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transportasi extends Model
{
    protected $table = 't_transportasi';
    protected $fillable = ['id_pemilikKendaraan','kategori','kapasitas','biayaperkilometer','status','dari','ke','jenis_kendaraan'];
}
