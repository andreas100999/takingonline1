<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pemesan extends Model
{
    protected $table = 't_user';
    protected $fillable = ['nama','email','password','no_identitas','saldo'];
}
