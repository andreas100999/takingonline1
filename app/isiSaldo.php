<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class isiSaldo extends Model
{
    protected $table = 't_pengisian';
    protected $fillable = ['id_user','total','buktiPengisian'];
}
