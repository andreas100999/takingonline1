<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rute extends Model
{
    protected $table = 't_rute';
    protected $fillable = ['dari','biaya_km','ke'];
}
