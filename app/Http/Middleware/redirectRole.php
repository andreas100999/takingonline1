<?php

namespace App\Http\Middleware;
use Auth;
use Session;
use Closure;

class redirectRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->level == 'admin'){
          return redirect('admin');
        }
        else if(Auth::user()->level == 'pemesan'){
          return redirect('pemesan');
        }
        else if(Auth::user()->level == 'pemilik'){
          Session::put('pemilik');
          return redirect('pemilik');
        }
    }
}
