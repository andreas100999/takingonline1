<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use App\pemesanan;
use App\rute;
use App\transportasi;
use Illuminate\Http\Request;

class prosesPesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $dari = $request->dari;
      $ke = $request->ke;
      $jenis =$request->jenis;
      $id =DB::Select('select * from t_transportasi where kategori=? AND dari=? AND ke=? AND status=?',[$jenis,$dari,$ke,1]);
      $id_rute = DB::table('t_rute')->select('id')->where('dari', $dari)->where('ke',$ke)->first();


       return view('taking.lanjutPesan',compact('id'));
    }

    public function simpan(Request $request){
      $dari = $request->dari;
      $ke = $request->ke;
      $jenis =$request->jenis;
      $id =DB::Select('select * from t_transportasi where kategori=? AND dari=? AND ke=? AND status=?',[$jenis,$dari,$ke,1]);
      $id_rute = DB::table('t_rute')->select('id')->where('dari', $dari)->where('ke',$ke)->first();


       return view('taking.lanjutRental',compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$id_pemilikKendaraan)
    {
        $id_user = Auth::user()->id;
        $id_transportasi = $id;
        $trans = transportasi::where('id',$id)->get();
        $dari =$trans[0]['dari'];
        $ke =$trans[0]['ke'];
        $biaya = $trans[0]['biayaperkilometer'];
        $rutes = rute::where('dari',$dari)->where('ke',$ke)->get();
        // $biayaPerkiloMeter = DB::table('t_transportasi')->select('biayaperkilometer')->where('id_pemilikKendaraan', $id_pemilikKendaraan)->first();
        // $dari = DB::table('t_transportasi')->select('dari')->where('id_pemilikKendaraan', $id_pemilikKendaraan)->get();
        // $ke = DB::table('t_transportasi')->select('ke')->where('id_pemilikKendaraan', $id_pemilikKendaraan)->first();
        //$km = DB::table('t_rute')->select('km')->where('dari', $dari)->where('ke',$ke)->first();
        //$post = rute::where('dari','silangit')->where('ke','balige')->get();
        $km = $rutes[0]['km'];
        $total = $biaya * $km;
        print_r($total);

        $simpan = new pemesanan;

        $simpan->id_user = $id_user;
        $simpan->id_pemilikKendaraan = $id_pemilikKendaraan;
        $simpan->id_transportasi = $id_transportasi;
        $simpan->biaya = $total;
        $simpan->save();

        return redirect('/coba');
        //print_r($post[0]['dari']);
    }
    public function store2($id,$id_pemilikKendaraan){
        print_r($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
