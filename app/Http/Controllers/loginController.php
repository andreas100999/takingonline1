<?php

namespace App\Http\Controllers;
use App\admin;
use App\pemesan;
use Auth;
use Session;
use Illuminate\Http\Request;

class loginController extends Controller
{
  function masuk(Request $kiriman){
        $email = $kiriman->email;

        $data = pemesan::where('email',$email)->first();
        $dat = admin::where('email',$email)->first();

    	$data1 = admin::where('email',$kiriman->email)->where('password',$kiriman->password)->get();

    	$data2 = pemesan::where('email',$kiriman->email)->where('password',$kiriman->password)->get();

    	if(count($data1)>0){
    		Auth::guard('admin')->LoginUsingId($data1[0]['id']);
            Session::put('email',$dat->email);
            Session::put('login',TRUE);
			return redirect('/admin');
    	}else if(count($data2)>0){
    		Auth::guard('pemesan')->LoginUsingId($data2[0]['id']);
            Session::put('email',$data->email);
            Session::put('nama',$data->nama);
            Session::put('login',TRUE);
			return redirect('/');
    	}
    	else{
    		return 'login gagal';
    	}
    }

    function keluar(){
        Session::flush();
        return redirect('/')->with('alert','Kamu sudah logout');
    }

    function create(Request $data)
    {
        $data->validate([
            'nama'=>'required',
            'email'=>'required',
            'password'=>'required',
            'no_identitas'=>'required',
            'saldo'=>'required',
        ]);
        pemesan::create($data->all());

        return redirect('/');
    }

}
