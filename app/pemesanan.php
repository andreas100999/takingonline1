<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pemesanan extends Model
{
    protected $table = 't_pemesanan';
    protected $fillable = ['id_user','id_pemilikKendaraan','id_transportasi','biaya'];
}
